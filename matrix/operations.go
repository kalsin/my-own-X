package matrix

import (
	"errors"
	"fmt"
	"sync"
	"time"
)

func Multiply(m1, m2 [][]int) ([][]int, error) {
	row_m1 := len(m1)
	row_m2 := len(m2)
	column_m1 := len(m1[0])
	column_m2 := len(m2[0])

	if column_m1 != row_m2 {
		err := errors.New("число столбцов в первом сомножителе не равно числу строк во втором!")
		return nil, err
	}

	result := newMatrix(row_m1, column_m2)
	start := time.Now()
	for i := 0; i < row_m1; i++ {
		for j := 0; j < column_m2; j++ {
			result[i][j] = 0
			for k := 0; k < column_m1; k++ {
				result[i][j] += m1[i][k] * m2[k][j]
			}
		}
	}
	fmt.Printf("Multiply matrix took %v:\n", time.Since(start).Microseconds())
	return result, nil
}

func ParallelMultiply(m1, m2 [][]int) ([][]int, error) {
	row_m1 := len(m1)
	row_m2 := len(m2)
	column_m1 := len(m1[0])
	column_m2 := len(m2[0])

	if column_m1 != row_m2 {
		err := errors.New("число столбцов в первом сомножителе не равно числу строк во втором!")
		return nil, err
	}

	result := newMatrix(row_m1, column_m2)

	start := time.Now()

	wg := new(sync.WaitGroup)
	for i := 0; i < row_m1; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			for j := 0; j < column_m2; j++ {
				result[i][j] = 0
				for k := 0; k < column_m1; k++ {
					result[i][j] += m1[i][k] * m2[k][j]
				}
			}
		}(i)
	}
	wg.Wait()
	fmt.Printf("Multiply matrix parallel took %v:\n", time.Since(start).Microseconds())

	return result, nil
}

func Transpose(m [][]int) [][]int {
	row := len(m)
	column := len(m[0])
	result := newMatrix(column, row)

	for i := 0; i < row; i++ {
		for j := 0; j < column; j++ {
			result[j][i] = m[i][j]
		}
	}

	return result
}

func TransposeParallel(m [][]int) [][]int {
	row := len(m)
	column := len(m[0])
	result := newMatrix(column, row)

	wg := new(sync.WaitGroup)
	for i := 0; i < row; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			for j := 0; j < column; j++ {
				result[j][i] = m[i][j]
			}
		}(i)
	}

	wg.Wait()
	return result
}
