package matrix

import (
	"fmt"
	"math/rand"
	"sync"
)

func Print(m [][]int) {
	for i := 0; i < len(m); i++ {
		fmt.Println(m[i])
	}
}

func newMatrix(row, column int) [][]int {
	result := make([][]int, row)

	wg := new(sync.WaitGroup)
	for i := range result {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			result[i] = make([]int, column)
		}(i)
	}

	wg.Wait()
	return result
}

func RandMatrix(row, column int) [][]int {
	result := newMatrix(row, column)

	wg := new(sync.WaitGroup)
	for i := 0; i < row; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			for j := 0; j < column; j++ {
				result[i][j] = rand.Intn(100)
			}
		}(i)
	}
	
	wg.Wait()
	return result
}
