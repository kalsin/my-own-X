package main

import (
	"fmt"
	"math/rand"
	"my-own-X/sort"
	"sync"
	"time"
)

func main() {
	size := 10
	arr := make([]int, size)

	wg := new(sync.WaitGroup)
	for i := 0; i < size; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			arr[i] = rand.Intn(1000)
		}(i)
	}
	wg.Wait()
	start := time.Now()
	fmt.Println(sort.QuickSort(arr))
	fmt.Printf("Transpose took %v:\n", time.Since(start))
}

// func main() {
// 	m1 := matrix.RandMatrix(6000, 8800)
// 	start := time.Now()
// 	matrix.Transpose(m1)
// 	fmt.Printf("Transpose took %v:\n", time.Since(start))
// 	start = time.Now()
// 	matrix.TransposeParallel(m1)
// 	fmt.Printf("Transpose took %v:\n", time.Since(start))
// }

// func main() {
// 	start := time.Now()
// 	fmt.Println(classicalgorithms.Fib(100))
// 	fmt.Printf("Fibonachi took %v:\n", time.Since(start))

// 	start = time.Now()
// 	fmt.Println(classicalgorithms.FibIterate(100))
// 	fmt.Printf("Fibonacci iterate took %v:\n", time.Since(start))
// }

// func main() {
// 	m1 := matrix.RandMatrix(3, 4)

// 	m2 := matrix.RandMatrix(4, 5)

// 	result, err := matrix.Multiply(m1, m2)
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	matrix.Print(result)

// 	result, err = matrix.ParallelMultiply(m1, m2)
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	matrix.Print(result)

// 	fmt.Println("Big data:")
// 	m1 = matrix.RandMatrix(1000, 400)
// 	m2 = matrix.RandMatrix(400, 5000)

// 	_, err = matrix.Multiply(m1, m2)
// 	if err != nil {
// 		fmt.Println(err)
// 	}

// 	_, err = matrix.ParallelMultiply(m1, m2)
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// }
