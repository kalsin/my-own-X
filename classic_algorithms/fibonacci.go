package classicalgorithms

var fibCache = make(map[uint]uint64)

func Fib(n uint) uint64 {
	if n <= 2 {
		return 1
	}

	var result uint64
	if result, ok := fibCache[n]; ok {
		return result
	}

	result = Fib(n-2) + Fib(n-1)

	fibCache[n] = result

	return result
}

func FibIterate(n int) int64 {
	var x, y int64 = 1, 1
	for i := 1; i < n; i++ {
		x, y = y, x+y
	}
	return x
}
